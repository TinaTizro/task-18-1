//index.js
const express = require("express");
const app = express();
const { PORT = 3000 } =process.env;
const path = require("path");
const routes = express.Router();

app.use(express.static('public'));



//Routes Definitions
app.get("/", (req, res)=> {
    return res.sendFile(path.join(__dirname, 'public','index.html'));
   
});

app.get("/contact", (req, res)=> {
    return res.sendFile(path.join(__dirname, 'public', 'contact.html'));
});
app.get("/about", (req, res)=> {
    return res.sendFile(path.join(__dirname, 'public', 'about.html'));
});
app.get("/menu", (req, res)=> {
    return res.sendFile(path.join(__dirname, 'public', 'menu.html'));
});
app.get("/home", (req, res)=> {
    return res.sendFile(path.join(__dirname, 'public', 'home.html'));
});
app.listen(PORT, ()=> console.log(`server started on port ${PORT}... `))